<?php get_header(); ?>

    <!-- MAIN -->
        <main id="container-main-home" class="container-main">
        <!-- Sub-grid-->
        
        <!-- HERO -->
            <div class="hero hero1" data-aos="fade-right" data-aos-duration="2000">
                <img class="img-responsiva" src="<?php bloginfo('template_url'); ?>/img/logo-11-cnp-simbolo.png" alt="11º" />
            </div>
            <div class="hero hero2">
                <h1  data-aos="fade-left" data-aos-duration="2000" data-aos-delay="1000">
                    <img class="img-responsiva" src="<?php bloginfo('template_url'); ?>/img/logo-11-cnp-texto.png" alt="11 Congresso Nacional da Psicologia">
                </h1>

                <h2 data-aos="fade-left" data-aos-duration="2000" data-aos-delay="1500">O Impacto Psicossocial da Pandemia:</h2>
                <p class="cnp-subtitulo-hero-home" data-aos="fade-left" data-aos-duration="2000" data-aos-delay="2000">Desafios e Compromissos para a Psicologia Brasileira Frente às Desigualdades Sociais</p>

                <a href="https://cnp.cfp.org.br/11/sobre/o-que-e/" class="btn-hero-home " data-aos="fade-left" data-aos-duration="2000" data-aos-delay="2500">Saiba mais</a>
            </div>

        <!-- 3 BOX DESTAQUE-->
            <div class="container-box-destaques">
                <div class="box-home box-home1">
                    <a href="https://cnp.cfp.org.br/11/11-cnp/regulamento/">
                        <h2><i class="fas fa-file-alt"></i> Regulamento</h2>
                        <p class="p-home">Fique por dentro de como vai funcionar o 11º CNP: conheça as normas e orientações</p>
                    </a>
                </div>
                <div class="box-home box-home2">      
                    <a href="https://site.cfp.org.br/wp-content/uploads/2021/07/11CNP_texto_orientador.pdf" target="_blank">
                        <h2><i class="fas fa-compass"></i> Texto orientador</h2>
                        <p class="p-home">Pelo sistema de envio você também pode mandar a sua proposta para o seu CRP</p>
                    </a>
                </div>
                <div class="box-home box-home3">
                    <a href="https://cnp.cfp.org.br/11/eventos/crps/" target="_blank">
                        <h2><i class="fas fa-calendar-check"></i> Eventos</h2>
                        <p class="p-home">Confira todos os eventos relacionados ao Congresso Nacional de Psicologia que estão ocorrendo pelo Brasil</p>
                    </a>
                </div>
            </div>

        <!-- ENTENDA O CNP -->
            <div class="video-home video-home1" data-aos="fade-up" data-aos-duration="3000">
                <span class="barra-divisoria"></span>
                <h2 class="h2-home">Entenda o Congresso</h2>
                <p class="p-home">O Congresso Nacional de Psicologia é a instância máxima de deliberação do Sistema Conselhos de Psicologia, que agrega o Conselho Federal de Psicologia (CFP) e os 24 Conselhos Regionais de Psicologia (CRP).</p>
            </div>
            <div class="video-home video-home2" data-aos="flip-up" data-aos-duration="3000">
                <iframe class="video-responsivo" width="536" height="302" src="https://www.youtube.com/embed/j0DZy_wVvkc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        
        <?php //get_template_part( 'fique-por-dentro' ); //fique-por-dentro.php ?>
  
        
        <!-- CERTIFICADO -->
            <div class="certificado-home certificado-home1" data-aos="flip-down" data-aos-duration="3000">
                <img class="img-responsiva" src="<?php bloginfo('template_url'); ?>/img/certificado-home.png" alt="Certificado"/>
            </div>
            <div class="certificado-home certificado-home2" data-aos="fade-up" data-aos-duration="3000">
                <span class="barra-divisoria"></span>
                <h2 class="h2-home">Garanta seu certificado</h2>
                <p class="p-home">Participe do 11º Congresso Nacional de Psicologia (CNP). Ao fim do evento, a organização emitirá certificados de participação aos congressistas.</p>
            </div>

        </main>

<?php get_footer(); ?>